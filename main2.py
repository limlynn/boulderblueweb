import tornado.ioloop
import tornado.web
import sys
import json
from tornado.options import options, define, parse_command_line
import logging
import tornado.httpserver
import tornado.web
import tornado.wsgi
from tornado.platform.asyncio import to_tornado_future

from concurrent.futures import ThreadPoolExecutor

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "BoulderBlueServerWebApp.BoulderBlueServerWebApp.settings")

import django
django.setup()
from django.core.wsgi import get_wsgi_application


#from bluetooth.ble import DiscoveryService, GATTRequester
#import bluetooth

#service = DiscoveryService()
# FE:59:2F:D0:BE:DC Mouse2
# CF:3C:6B:C6:40:4A Mouse1


from django.core.exceptions import ObjectDoesNotExist
from BoulderBlueServerWebApp.BoulderBlueServerWebApp import settings as djsettings
from BoulderBlueServerWebApp.blue_views.models import *
import datetime
from django.db import transaction
from functools import wraps

import pygatt
from pygatt.backends import BGAPIBackend, Characteristic, BLEAddressType
#import logging
#logging.basicConfig()
#logging.getLogger('pygatt').setLevel(logging.DEBUG)

THREADPOOL_MAX_WORKERS = 10
THREADPOOL_TIMEOUT_SECS = 30
from tornado import gen
from datetime import timedelta
from tornado.concurrent import run_on_executor
import types



def onthread(function):
    @gen.coroutine
    def decorated(self, *args, **kwargs):
        future = executed(self, *args, **kwargs)
        try:
            response = yield gen.with_timeout(
                timedelta(seconds=THREADPOOL_TIMEOUT_SECS), future)
            if isinstance(response, types.GeneratorType):  # subthreads
                response = yield gen.with_timeout(
                    timedelta(seconds=THREADPOOL_TIMEOUT_SECS),
                    next(response))
        except gen.TimeoutError as exc:
            future.cancel()
            raise exc


    @run_on_executor
    def executed(*args, **kwargs):
        return function(*args, **kwargs)

    return decorated

from BoulderBlueServerWebApp.blue_views import functions


import sys
import glob
import serial


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/ttyACM*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result



class BluetoothHandler(object):
    ble_backend = None
    ADDRESS_TYPE = pygatt.BLEAddressType.random
    is_connected = False
    is_scanning = False
    connected_devices = {}

    executor = ThreadPoolExecutor(max_workers=THREADPOOL_MAX_WORKERS)

    def start_ble_backend(self):

        port = serial_ports()[1]
        self.ble_backend = BGAPIBackend(serial_port=port)
        self.ble_backend.start()

        


    @onthread
    def discover_in_background(self):
        self.discover()

    def discover(self, duration=None):
        if self.is_connected == True or self.is_scanning == True:
            return

        self.is_scanning = True
        try:
            if duration is None:
                duration = 6
            devices = self.ble_backend.scan(timeout=duration)
            for device in devices:
                print("Device %s \n%s\n\n" % (device['name'], device))
            self.check_registrations(devices)

            self.connect_to_registered_devices()

        except Exception as e:
            print("Error while scanning: %s" % e)

        self.is_scanning = False


    def check_registrations(self, devices):
        # Mouse 1 CF:3C:6B:C6:40:4A
        # Mouse 2 FE:59:2F:D0:BE:DC

        with transaction.atomic():
            for reg in RegisteredChip.objects.all():
                found = False
                for device in devices:
                    address = device['address']
                    if reg.mac == address:
                        found = True
                        reg.last_seen = datetime.datetime.now()
                        reg.visible = True
                        reg.save()

                if found == False:
                    reg.visible = False
                    reg.save()

        with transaction.atomic():

            for device in devices:
                address = device['address']
                try:
                    RegisteredChip.objects.get(mac=address)
                    DiscoveredChip.objects.get(mac=address).delete()
                    continue
                except ObjectDoesNotExist as e:
                    pass

                found = False
                for dis in DiscoveredChip.objects.all():
                    if dis.mac == address:
                        found = True
                        dis.last_seen = datetime.datetime.now()
                        dis.visible = True
                        dis.name = device['name']
                        dis.save()

                if found == False:
                    try:
                        RegisteredChip.objects.get(mac=address)

                    except ObjectDoesNotExist as e:
                        dis = DiscoveredChip()
                        dis.mac = address
                        dis.name = device['name']
                        dis.visible = True
                        dis.last_seen = datetime.datetime.now()
                        dis.save()

        with transaction.atomic():
            for dis in DiscoveredChip.objects.all():
                found = False
                for device in devices:
                    address = device['address']
                    if dis.mac == address:
                        found = True

                if found == False:
                    dis.delete()

    def connect_to_device_if_necessary(self, mac):
        try:
            if mac not in self.connected_devices:
                connected_device = self.ble_backend.connect(address=mac, address_type=self.ADDRESS_TYPE, timeout=10)
                print("connected device %s" % connected_device)
                connected_device.discover_characteristics()
                #connected_device.bond()
                dic = {}

                dic['device'] = connected_device
                self.connected_devices[mac] = dic

        except Exception as e:
            print("Exception when connecting to device %s" % e)
            return e

        return None

    def connect_to_registered_devices(self):
        devices = RegisteredChip.objects.filter(visible=True)

        for device in devices:
            ex = self.connect_to_device_if_necessary(device.mac)
            if ex:
                print("ERROR RECONNECTING TO DEVICE connect after scan:::::::::::::::::::: %s %s" % (device.mac, ex))




    def send_commands(self, device_commands, scheduled=False):
        logs = []

        for item in device_commands:
            mac = item['mac']
            commands = item['commands']
            self.is_connected = True
            ex = self.connect_to_device_if_necessary(mac)
            if ex:
                print("ERROR RECONNECTING TO DEVICE send command :::::::::::::::::::: %s %s" % (mac, ex))

            reg_chip = RegisteredChip.objects.get(mac=mac)

            for i, command in enumerate(commands):
                log = CommandLog()
                log.send_index = i
                log.registered_chip = reg_chip
                log.command = command
                log.scheduled = scheduled
                log.save()
                logs.append(log)


            if mac not in self.connected_devices:
                for i, command in enumerate(commands):
                    logs[i].error = "Device is not connected: %s" % mac

                return logs

            connected_device = self.connected_devices[mac]['device']
            try:
                for i, command in enumerate(commands):

                    try:
                        value = ord(command.command)
                        print("Wait for response")
                        a = connected_device.char_write_handle(char_handle=0x11, value=[value], wait_for_response=True)
                        print("result %s" % a)
                        logs[i].successful = True

                    except Exception as e:
                        if "[<EventPacketType.attclient_procedure_completed: 17>]"  in "%s" % e:
                            logs[i].successful = True

                            continue
                        logs[i].error = "Exception %s" % e
                        print("1 Error sending command %s to %s: %s" % (command.command, mac, e))


            except Exception as e:
                if "[<EventPacketType.attclient_procedure_completed: 17>]" not in "%s" % e:

                    for i, command in enumerate(commands):
                        logs[i].error = "Exception: %s" % e


            # if connected_device:
            #     self.ble_backend.clear_bond(mac)
            self.is_connected = False

            for log in logs:
                log.save()

            # connected_device.disconnect()
            # self.connected_devices.pop(mac)


            print("Logs: %s", logs)
        return logs

    def check_for_executable_commands(self):
        functions.run_scheduled_commands_if_necessary()




def main():
    #logger = logging.getLogger(__name__)
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler())


    tornado_app = tornado.web.Application(
        [

            #(r'/static/admin/(.*)', tornado.web.StaticFileHandler, {'path': '/root/.virtenvs/boulder_blue/lib/python3.4/site-packages/django/contrib/admin/static/admin/'}),
            (r'/static/(.*)', tornado.web.StaticFileHandler,
             {'path': '/srv/www/BoulderBlueServer/BoulderBlueServerWebApp/BoulderBlueServerWebApp/static/'}),

            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ], debug=True)
    #logger.info("Tornado server starting...")

    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(8889)


    bhandler = BluetoothHandler()
    bhandler.start_ble_backend()
    djsettings.BLUETOOTHHANDLER = bhandler
    import time
    time.sleep(2)
    bhandler.discover()
    main_loop = tornado.ioloop.IOLoop.instance()



    search = tornado.ioloop.PeriodicCallback(bhandler.discover_in_background, 30000, io_loop=main_loop)

    search.start()


    sched_commands = tornado.ioloop.PeriodicCallback(bhandler.check_for_executable_commands, 100, io_loop=main_loop)
    sched_commands.start()
    print("started Tornado!")
    main_loop.start()



if __name__ == "__main__":
    main()
