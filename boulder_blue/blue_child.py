from zmq.eventloop import ioloop
ioloop.install()

import tornado.web
import sys
import tornado.httpserver
import tornado.web
import tornado.wsgi

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "BoulderBlueServerWebApp.BoulderBlueServerWebApp.settings")

import django
django.setup()
from django.core.wsgi import get_wsgi_application

from BoulderBlueServerWebApp.BoulderBlueServerWebApp import settings as djsettings

serial_port = None
from communicator import ZMQ_Communicator
from bt_handler import BluetoothClient
import datetime

main_loop = None


def main():
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler())


    tornado_app = tornado.web.Application(
        [

            #(r'/static/admin/(.*)', tornado.web.StaticFileHandler, {'path': '/root/.virtenvs/boulder_blue/lib/python3.4/site-packages/django/contrib/admin/static/admin/'}),
            (r'/static/(.*)', tornado.web.StaticFileHandler,
             {'path': '/srv/www/BoulderBlueServer/BoulderBlueServerWebApp/BoulderBlueServerWebApp/static/'}),

            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ], debug=True)
    #logger.info("Tornado server starting...")
    args = sys.argv[1:]
    serial_port = args[0]
    port_index = int(args[1])
    print("serial port %s" % serial_port)

    djsettings.BLUETOOTH_CLIENT_SERIAL_PORT = serial_port

    communicator =  ZMQ_Communicator()
    communicator.start()
    communicator.setup_publish_child(port_index)
    bluetooth_client = BluetoothClient(serial_port, communicator)
    djsettings.COMMUNICATOR = communicator
    djsettings.BLUETOOTH_CLIENT = bluetooth_client

    communicator.setup_subscriber_to_srv(bluetooth_client)

    global main_loop

    main_loop = ioloop.IOLoop.instance()

    def start_scheduler():
        print("START Availability scheduler")
        scheduled = tornado.ioloop.PeriodicCallback(bluetooth_client.check_for_device_availability, 30000,
                                                    io_loop=main_loop)
        scheduled.start()

    main_loop.add_timeout(datetime.timedelta(seconds=20), start_scheduler)



    main_loop.start()

if __name__ == "__main__":
    main()
