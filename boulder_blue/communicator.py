PUB_SUB_PORT_MAIN = 5555
PUB_SUB_PORTS_CHILD = [5557, 5558, 5559, 5560, 5561, 5562, 5563, 5564,5565, 5567, 5568]
PUSH_POLL_PORTS_CHILD = [5657, 5658, 5659, 5660, 5661, 5662, 5663, 5664,5665, 5667, 5668]

import zmq
import json
import threading
from zmq.eventloop import ioloop, zmqstream


class ZMQ_Communicator(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.daemon = True

    def setup_publish_main(self):
        self.pub_sub_context_main = zmq.Context()
        self.pub_sub_socket_main = self.pub_sub_context_main.socket(zmq.PUB)
        self.pub_sub_socket_main.bind("tcp://*:%s" % PUB_SUB_PORT_MAIN)

    def publish_main(self, dic):
        print("publish main %s" % self.pub_sub_socket_main)
        if self.pub_sub_socket_main:
            self.pub_sub_socket_main.send(bytes("from_srv ||%s" % json.dumps(dic), encoding="utf-8"))


    def setup_subscriber_to_srv(self, target):

        context = zmq.Context()
        socket_sub = context.socket(zmq.SUB)
        socket_sub.connect("tcp://localhost:%s" % PUB_SUB_PORT_MAIN)
        socket_sub.setsockopt(zmq.SUBSCRIBE, b"from_srv")
        stream_sub = zmqstream.ZMQStream(socket_sub)
        stream_sub.on_recv(target.process_zmq_message_from_srv)


    def setup_publish_child(self, index):

        self.pub_sub_context_child = zmq.Context()
        self.pub_sub_socket_child = self.pub_sub_context_child.socket(zmq.PUB)
        self.pub_sub_socket_child.bind("tcp://*:%s" % PUB_SUB_PORTS_CHILD[index])


        self.push_pull_context_child = zmq.Context()
        self.push_pull_socket_child = self.push_pull_context_child.socket(zmq.PUSH)
        self.push_pull_socket_child.bind("tcp://*:%s" % PUSH_POLL_PORTS_CHILD[index])

    def publish_child(self, dic):
        if self.pub_sub_socket_child:
            self.pub_sub_socket_child.send(bytes("from_chld ||%s" % json.dumps(dic), encoding="utf-8"))

    def publish_child_push(self, dic):
        if self.push_pull_socket_child:
            self.push_pull_socket_child.send(bytes("from_chld ||%s" % json.dumps(dic), encoding="utf-8"))

    def setup_subscriber_to_child(self, target):
        self.from_child_target = target
        print("setup_subscriber_to_child")
        for p in PUB_SUB_PORTS_CHILD:
            context = zmq.Context()
            socket_sub = context.socket(zmq.SUB)
            socket_sub.connect("tcp://localhost:%s" % p)
            socket_sub.setsockopt(zmq.SUBSCRIBE, b"from_chld")
            stream_sub = zmqstream.ZMQStream(socket_sub)
            stream_sub.on_recv(target.process_zmq_message_from_child)

        print("setup_subscriber_to_child")
        self.pollers = []
        self.pull_sockets = []
        for p in PUSH_POLL_PORTS_CHILD:
            context = zmq.Context()
            socket_pull = context.socket(zmq.PULL)
            socket_pull.connect("tcp://localhost:%s" % p)

            poller = zmq.Poller()
            poller.register(socket_pull, zmq.POLLIN)

            self.pollers.append(poller)
            self.pull_sockets.append(socket_pull)

    def poll(self):
        for poller in self.pollers:
            socks = dict(poller.poll(100))

            for pull_socket in self.pull_sockets:
                if pull_socket in socks:
                    message = pull_socket.recv()
                    self.from_child_target.process_zmq_message_from_child_any(str(message, 'utf-8'))