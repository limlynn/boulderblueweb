from zmq.eventloop import ioloop

ioloop.install()

import tornado.web

import tornado.httpserver
import tornado.web
import tornado.wsgi

import threading

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "BoulderBlueServerWebApp.BoulderBlueServerWebApp.settings")

import django

django.setup()
from django.core.wsgi import get_wsgi_application

from BoulderBlueServerWebApp.BoulderBlueServerWebApp import settings as djsettings

import subprocess
from multiprocessing import Process

import sys
import glob
import serial

from bt_handler import BluetoothHandler, BluetoothServer

from communicator import ZMQ_Communicator
import datetime

MAX_MICE_PER_DONGLE = 4

main_loop = None


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/ttyACM*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def launch_sub_process(dir_path, serial_port, port_index):
    print("launching %s" % subprocess.call(["python", "%s/blue_child.py" % dir_path, serial_port, '%s' % port_index]))


def main():
    # logger = logging.getLogger(__name__)
    wsgi_app = tornado.wsgi.WSGIContainer(
        django.core.handlers.wsgi.WSGIHandler())

    print("START START")
    tornado_app = tornado.web.Application(
        [

            # (r'/static/admin/(.*)', tornado.web.StaticFileHandler, {'path': '/root/.virtenvs/boulder_blue/lib/python3.4/site-packages/django/contrib/admin/static/admin/'}),
            (r'/static/(.*)', tornado.web.StaticFileHandler,
             {'path': '/srv/www/BoulderBlueServer/BoulderBlueServerWebApp/BoulderBlueServerWebApp/static/'}),

            ('.*', tornado.web.FallbackHandler, dict(fallback=wsgi_app)),
        ], debug=True)
    # logger.info("Tornado server starting...")

    server = tornado.httpserver.HTTPServer(tornado_app)
    server.listen(8888)

    ports = serial_ports()
    dir_path = os.path.dirname(os.path.realpath(__file__))

    import time

    dongles = 0

    for i, p in enumerate(ports):
        if i == 0:
            bhandler = BluetoothHandler(None)
            bhandler.start_ble_backend(p)
            time.sleep(3)
        else:
            prc = Process(target=launch_sub_process, args=(dir_path, p, i))
            time.sleep(3)
            prc.start()
            dongles += 1

    djsettings.BLUETOOTH_HANDLER = bhandler
    import time
    time.sleep(1)

    global main_loop

    main_loop = ioloop.IOLoop.instance()

    search = tornado.ioloop.PeriodicCallback(bhandler.discover_in_background, 30000, io_loop=main_loop)

    search.start()

    communicator = ZMQ_Communicator()
    bluetooth_server = BluetoothServer(communicator, dongles, MAX_MICE_PER_DONGLE)

    communicator.setup_publish_main()
    communicator.start()

    djsettings.COMMUNICATOR = communicator
    djsettings.BLUETOOTH_SERVER = bluetooth_server

    bhandler.discover()

    def start_scheduler():
        sched_commands = tornado.ioloop.PeriodicCallback(bhandler.check_for_executable_commands, 300, io_loop=main_loop)
        sched_commands.start()

    main_loop.add_timeout(datetime.timedelta(seconds=20), start_scheduler)

    def setup_things():
        bluetooth_server.request_client_serial_port_responses()

    main_loop.add_timeout(datetime.timedelta(seconds=2), setup_things)

    def initialize_client_support():
        print("initialize client support")

        bluetooth_server.request_clients_to_connect_to_existing_devices()

    print("prepare initialize client support")

    main_loop.add_timeout(datetime.timedelta(seconds=5), initialize_client_support)

    def setup_child():
        communicator.setup_subscriber_to_child(bluetooth_server)

    main_loop.add_timeout(datetime.timedelta(seconds=1), setup_child)

    print("started Tornado!")

    main_loop.start()


if __name__ == "__main__":
    main()
