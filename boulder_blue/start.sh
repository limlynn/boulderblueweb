#!/bin/bash

source /root/.virtenvs/boulder_blue/bin/activate

cd /srv/www/BoulderBlueServer

python /srv/www/BoulderBlueServer/main.py > log.log 2>&1