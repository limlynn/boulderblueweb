from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, asynchronous
from tornado import gen
import tornado
from datetime import timedelta
from tornado.concurrent import run_on_executor
import types
import random
from django.core.exceptions import ObjectDoesNotExist
from BoulderBlueServerWebApp.blue_views.models import *
import datetime
from django.db import transaction

import pygatt
from pygatt.backends import BGAPIBackend
from concurrent.futures import ThreadPoolExecutor
from BoulderBlueServerWebApp.blue_views import functions
import json

import math
import time

from uuid import uuid4
from zmq.eventloop import ioloop
from tornado.locks import Condition

condition = Condition()

THREADPOOL_MAX_WORKERS = 10
THREADPOOL_TIMEOUT_SECS = 30
from tornado.httpclient import AsyncHTTPClient

import threading


def onthread(function):
    @gen.coroutine
    def decorated(self, *args, **kwargs):
        future = executed(self, *args, **kwargs)
        try:
            response = yield gen.with_timeout(
                timedelta(seconds=THREADPOOL_TIMEOUT_SECS), future)
            if isinstance(response, types.GeneratorType):  # subthreads
                response = yield gen.with_timeout(
                    timedelta(seconds=THREADPOOL_TIMEOUT_SECS),
                    next(response))
        except gen.TimeoutError as exc:
            future.cancel()
            raise exc

    @run_on_executor
    def executed(*args, **kwargs):
        return function(*args, **kwargs)

    return decorated


class BluetoothHandler(object):
    ble_backend = None
    ADDRESS_TYPE = pygatt.BLEAddressType.random
    connected_devices = {}

    executor = ThreadPoolExecutor(max_workers=THREADPOOL_MAX_WORKERS)

    def __init__(self, comm_client):
        self.comm_client = comm_client
        super(BluetoothHandler, self).__init__()

    def start_ble_backend(self, serial_port):
        self.serial_port = serial_port
        self.ble_backend = BGAPIBackend(serial_port=serial_port)
        self.ble_backend.start()

    @onthread
    def discover_in_background(self):
        self.discover()

    def discover_do_nothing(self, duration=None):
        try:
            if duration is None:
                duration = 1
            devices = self.ble_backend.scan(timeout=duration)


        except Exception as e:
            print("Error while scanning: do nothing %s" % e)

    def discover(self, duration=None):

        try:
            if duration is None:
                duration = 6
            devices = self.ble_backend.scan(timeout=duration)
            # for device in devices:
            # print("Device %s \n%s\n\n" % (device['name'], device))
            self.check_registrations(devices)

        except Exception as e:
            print("Error while scanning: %s" % e)

    def check_registrations(self, devices):
        # Mouse 1 CF:3C:6B:C6:40:4A
        # Mouse 2 FE:59:2F:D0:BE:DC

        with transaction.atomic():
            for reg in RegisteredChip.objects.all():
                found = False
                for device in devices:
                    address = device['address']
                    if reg.mac == address:
                        found = True
                        reg.last_seen = datetime.datetime.now()
                        reg.visible = True
                        reg.save()

                if found == False:
                    reg.visible = False
                    reg.save()

        with transaction.atomic():

            for device in devices:
                address = device['address']
                try:
                    RegisteredChip.objects.get(mac=address)
                    DiscoveredChip.objects.get(mac=address).delete()
                    continue
                except ObjectDoesNotExist as e:
                    pass
                except Exception as e:
                    print("exception 1 check_registrations %s" % e)

                found = False
                for dis in DiscoveredChip.objects.all():
                    if dis.mac == address:
                        found = True
                        dis.last_seen = datetime.datetime.now()
                        dis.visible = True
                        dis.name = device['name']
                        dis.save()

                if found == False:
                    try:
                        RegisteredChip.objects.get(mac=address)

                    except ObjectDoesNotExist as e:
                        dis = DiscoveredChip()
                        dis.mac = address
                        dis.name = device['name']
                        dis.visible = True
                        dis.last_seen = datetime.datetime.now()
                        dis.save()
                    except Exception as e:
                        print("exception 2 check_registrations %s" % e)

        with transaction.atomic():
            for dis in DiscoveredChip.objects.all():
                found = False
                for device in devices:
                    address = device['address']
                    if dis.mac == address:
                        found = True

                if found == False:
                    dis.delete()

    def connect_to_devices_if_necessary(self, macs):

        conn_devices = {}
        exceptions_x = None
        print("already connected %s" % self.connected_devices)
        for mac in macs:
            try:
                if mac not in self.connected_devices:
                    try:
                        connected_device = self.ble_backend.connect(address=mac, address_type=self.ADDRESS_TYPE,
                                                                    timeout=10)
                        print("connected, checking characteristics")
                        connected_device.discover_characteristics()
                        # connected_device.bond()

                        dic = {}

                        dic['device'] = connected_device
                        dic['exception'] = ''
                        dic['mac'] = mac
                        conn_devices[mac] = dic
                        self.connected_devices[mac] = dic
                        print("Connected:::::::::::::::::::::::: %s" % dic)
                    except Exception as e:
                        print("Exception connecting to device if necessary %s %s" % (e, mac))
                        ex = str(e)
                        if ex is None or len(ex) == 0:
                            ex = 'unkown connection issue!!'
                        conn_devices[mac] = {'exception': ex, 'mac': mac}
                        raise e

            except Exception as e:
                if exceptions_x == None:
                    exceptions_x = []
                    exceptions_x.append(e)
                print("Exception when connecting to device %s %s" % (e, mac))

        return conn_devices, exceptions_x

    def check_command_for_reset(self, connected_device, mac, command):
        print("\n\n\nCHECKING FOR RESET:::::::::::::::::::: %s" % command.command)
        if command.command == 'r':
            print("disconnect!!!!!!!!!")
            connected_device.disconnect()
            self.connected_devices.pop(mac)

            time.sleep(1)
            conn_devices, ex = self.connect_to_devices_if_necessary([mac])

            if ex:
                print("Raising Exception")
                raise Exception(
                    "ERROR RECONNECTING TO DEVICE after [r] :::::::::::::::::::: %s %s" % (mac, ex))
            else:
                return True
        return False

    def disconnect_from_device(self, mac):
        print("Disconnecting from...... %s" % mac)

        try:
            if mac in self.connected_devices and 'device' in self.connected_devices[mac]:
                connected_device = self.connected_devices[mac]['device']
                connected_device.disconnect()
                self.connected_devices.pop(mac)
            else:
                print("NOTHING TO DISCONNECT FOR %s" % mac)

        except Exception as e:
            return e

        return None

    def reconnect_to_device(self, connected_device, mac):

        connected_device.disconnect()
        self.connected_devices.pop(mac)

        time.sleep(1)
        conn_devices, ex = self.connect_to_devices_if_necessary([mac])

        if ex:
            print("Raising Exception")
            raise Exception(
                "ERROR RECONNECTING TO DEVICE after failed command send :::::::::::::::::::: %s %s" % (mac, ex))

    # data 전송 성공 실패여부 확인
    def check_success(self, connected_device):
        handle_read = connected_device.get_handle('2d30c082-f39f-4ce6-923f-3484ea480596')
        print("Characteristics Read %s %s" % (handle_read, type(handle_read)))

        ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
        print("----------------------------------------------------------------")
        print("READ SOMETHING %s" % ret)
        ret = ret.decode()
        print("Decoded %s" % ret)
        # 문자열이 X로 시작하면 데이터 전송에 성공, 아니면 실패
        if ret.startswith("X"):
            return True  # 성공 반환

        return False  # 실패 반환

    # 데이터를 받아오는 함수
    def get_data_from_device(self, connected_device):
        try:
            handle_read = connected_device.get_handle('2d30c082-f39f-4ce6-923f-3484ea480596')
            print("Characteristics Read %s %s" % (handle_read, type(handle_read)))

            ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
            print("----------------------------------------------------------------")
            print("get_data_READ SOMETHING %s" % ret)
            read_data_from_chip = ret.decode()
            print("----------------------------------------------------------------")
            return read_data_from_chip

        except:
            print("Error in [get_data_from_device]")

    def get_data_from_device_2(self, connected_device):
        try:
            handle_read = connected_device.get_handle('2d30c082-f39f-4ce6-923f-3484ea480596')
            print("Characteristics Read %s %s" % (handle_read, type(handle_read)))

            ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
            print("----------------------------------------------------------------")
            print("get_data_READ SOMETHING %s" % ret)
            read_data_from_chip = ret[0]
            print("----------------------------------------------------------------")
            return read_data_from_chip
        except:
            print("Error in [get_data_from_device_2]")

    #
    # # 데이터 계속 받아오기 함수 - 새로 작성
    #     def get_data_from_device_without_command(self, connected_device):
    #         handle_read = connected_device.get_handle('2d30c082-f39f-4ce6-923f-3484ea480596')
    #         print("Characteristics Read %s %s" % (handle_read, type(handle_read)))
    #         # 데이터 받을 장소 만들기
    #
    #         new_logs = []
    #
    #         for i in range(10):
    #             new_log = ReceivedData()
    #
    #             new_log.send_index = i
    #             new_log.received_time = datetime.datetime.now()
    #             new_log.send_chip = connected_device['name']
    #
    #             ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
    #             ret = ret.decode()
    #
    #             new_log.t_result = ret
    #
    #             new_log.save()
    #             new_logs.append(new_log)
    #             print("get_data_from_device_without_command : %s" % new_logs)
    #
    #         #print("READ SOMETHING %s" % read_data_from_chip)
    #
    #         #if ret.startswith("X"):
    #         #    return True
    #         #로그 파일 만들기 /데이터베이스
    #
    #         #string out of data_from_chip
    #         #return string
    #
    #         return new_logs
    #
    #     def get_log(self, connected_device):
    #         handle_read = connected_device.get_handle('2d30c082-f39f-4ce6-923f-3484ea480596')
    #         print("Characteristics Read %s %s" % (handle_read, type(handle_read)))
    #         # 데이터 받을 장소 만들기
    #         #read_data_from_chip = []
    #
    #         ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
    #         print("----------------------------------------------------------------")
    #         print("READ SOMETHING %s" % ret)
    #         read_data_from_chip = ret.decode()
    #         print("Decoded %s" % read_data_from_chip)
    #         new_log = ReceivedData()
    #         new_log.received_time = datetime.datetime.now()
    #         new_log.send_chip = 'Who'
    #
    #         new_log.t_result = read_data_from_chip
    #
    #         new_log.save()
    #         new_logs.append(new_log)
    #         print("NEW_LOOOOOOOG : %s" % new_log)
    #
    #         return read_data_from_chip

    # BluetoothHandler
    def send_commands(self, device_commands, scheduled=False):
        logs_by_device = {}
        self.comm_client.check_for_device_availability()
        print("!!!!!!!!!!!!!!!!!!!!! Device Commands:::::::::::::::::::::")
        for item in device_commands:
            logs = []
            mac = item['mac']
            commands = item['commands']
            conns, exs = self.connect_to_devices_if_necessary([mac])
            if exs:
                print("ERROR RECONNECTING TO DEVICE send command :::::::::::::::::::: %s %s" % (mac, exs))

            reg_chip = RegisteredChip.objects.get(mac=mac)
            print("commands %s" % commands)
            for i, command_dic in enumerate(commands):
                print(command_dic['c'])
                command = ChipCommand.objects.get(id=command_dic['c'])
                log = CommandLog()
                log.send_index = i
                log.registered_chip = reg_chip
                log.command = command
                log.scheduled = scheduled

                if command.has_f and command_dic.get('f', '') != '':
                    log.f = int(command_dic['f'])

                if command.has_p and command_dic.get('p', '') != '':
                    log.p = int(command_dic['p'])

                if command.has_n and command_dic.get('n', '') != '':
                    log.n = int(command_dic['n'])

                log.save()
                logs.append(log)
                print("LOOOOOOOG : %s" % log)

            if mac not in self.connected_devices:
                self.comm_client.received_connection_request({self.comm_client.serial_port: [mac]})
                if mac not in self.connected_devices:
                    for i, command_dic in enumerate(commands):
                        logs[i].error = "Device is not connected: %s" % mac

                    log_pks = []
                    for log in logs:
                        log.save()
                        log.refresh_from_db()
                        log_pks.append(log.id)

                    print("log_pks %s" % log_pks)

                    logs_by_device[mac] = log_pks

                    continue

            print("con devices %s" % self.connected_devices)
            connected_device = self.connected_devices[mac]['device']
            try:
                for i, command_dic in enumerate(commands):
                    command = ChipCommand.objects.get(id=command_dic['c'])

                    command_string = command.command

                    if command.has_f and command_dic.get('f', '') != '':
                        command_string += 'F'
                        command_string += command_dic['f']
                        command_string += 'f'

                    if command.has_p and command_dic.get('p', '') != '':
                        command_string += 'P'
                        command_string += command_dic['p']
                        command_string += 'p'

                    if command.has_n and command_dic.get('n', '') != '':
                        command_string += 'N'
                        command_string += command_dic['n']
                        command_string += 'n'

                    print("command string %s" % command_string)

                    self.send_one_command(connected_device, command, command_string, mac, logs, i, 0)

            except Exception as e:
                import traceback
                just_the_string = traceback.format_exc()
                print("traceback %s" % just_the_string)
                print("2 Error sending command %s to %s: %s" % (command.command, mac, e))

                if "[<EventPacketType.attclient_procedure_completed: 17>]" not in "%s" % e:

                    for i, command in enumerate(commands):
                        logs[i].error = "Exception before sending: %s" % e

            log_pks = []
            for log in logs:
                log.save()
                log.refresh_from_db()
                log_pks.append(log.id)

            print("log_pks %s" % log_pks)
            logs_by_device[mac] = log_pks

        return logs_by_device

    # send_commands 에서 부르는 명령 한개 보내는 함수
    def send_one_command(self, connected_device, command, command_string, mac, logs, index, attempts=0):
        try:
            if self.connected_devices[mac]['exception'] != '':
                raise Exception("Previous connection issue: %s" % self.connected_devices[mac]['exception'])
            # 명령을 받아올 준비를 한다 한글자 한글자씩 분리하여 보낼 준비
            values = []

            for character in command_string:
                values.append(ord(character))

            values.append(0)

            print("SENDING::: %s %s" % (command.command, values))
            # 핸들 불러오기, 지정된 UUID 입력 write  = 83
            handle = connected_device.get_handle('2d30c083-f39f-4ce6-923f-3484ea480596')
            print("Characteristics %s %s" % (handle, type(handle)))
            # char handle 0x11
            # 접속한 장비에 문자를 입력(전송한다)
            connected_device.char_write_handle(char_handle=handle, value=values, wait_for_response=True)

            # 전송 성공을 확인한다. 여기서는 전송이 Y / X로 지정되어 있지만, 새로 고칠 함수에서는 Y, X, + data 로 들어와야한다.
            t_result = None

            if command.command == "T":
                # some string = self.get_data_from_device
                print("if command_string == T, you could see this line. ")
                t_result = self.get_data_from_device(connected_device)

                logs[index].t_result = t_result
                logs[index].successful = True
                logs[index].type_data = "Temp"

                print("LOOOOOOOG : %s" % logs[index])

            elif command.command == "P":
                # some string = self.get_data_from_device
                print("if command_string == P, you could see this line. ")
                t_result = self.get_data_from_device_2(connected_device)

                logs[index].t_result = t_result
                logs[index].successful = True
                logs[index].type_data = "Pressure"

                print("LOOOOOOOG : %s" % logs[index])

            elif command.command == "H":
                # some string = self.get_data_from_device
                print("if command_string == H, you could see this line. ")
                t_result = self.get_data_from_device_2(connected_device)

                logs[index].t_result = t_result
                logs[index].successful = True
                logs[index].type_data = "Humidity"

                print("LOOOOOOOG : %s" % logs[index])

            else:
                if self.check_success(connected_device) == False:
                    raise Exception("Failed to send command! %s" % command_string)

            self.check_command_for_reset(connected_device, mac, command)

        except Exception as ex:
            print("1 Error sending command %s to %s: %s" % (command.command, mac, ex))

            if "[<EventPacketType.attclient_procedure_completed: 17>]" in "%s" % ex:
                reseted = self.check_command_for_reset(connected_device, mac, command)

                if reseted == True:
                    logs[index].successful = True
                    return

            print("-------> Retrying command %s for %s" % (command_string, mac))

            try:
                self.reconnect_to_device(connected_device, mac)
                time.sleep(1)
            except Exception as e2:
                print("Exception trying to reconnect....... %s" % e2)
                pass

            if attempts < 3:
                print("Will try resending for attempt %s %s" % (str(attempts + 1), mac))
                self.send_one_command(connected_device, command, command_string, mac, logs, index, attempts + 1)
                return

            logs[index].error = "Exception for attempt %s sending %s" % (str(index), ex)

    def check_for_executable_commands(self):

        functions.run_scheduled_commands_if_necessary()


class BluetoothServer(object):

    def __init__(self, communicator, dongle_count, max_devices_per_dongle):
        self.responded_client_serial_ports = []
        self.connected_devices = {}
        self.sent_commands_u_ids = []
        self.communicator = communicator
        self.sent_commands_returned_logs = []

        self.dongle_count = dongle_count
        self.max_devices_per_dongle = max_devices_per_dongle

        super(BluetoothServer, self).__init__()

    def get_connected_macs(self):
        lists = self.connected_devices.values()
        conn_macs = []
        for l in lists:
            print("l %s" % l)
            for x in l:
                conn_macs.append(x['mac'])

        return conn_macs

    def serial_port_responded(self, serial_port):
        self.responded_client_serial_ports.append(serial_port)
        print("responded_client_serial_ports %s" % self.responded_client_serial_ports)

    def client_did_connect_to_devices(self, connect_dic):
        print("Received %s" % connect_dic)
        for serial_port in connect_dic.keys():
            if serial_port not in self.connected_devices:
                self.connected_devices[serial_port] = []

            conns = connect_dic[serial_port].values()

            for con in conns:
                mac = con['mac']
                found_index = None
                for i, dev in enumerate(self.connected_devices[serial_port]):
                    if dev['mac'] == mac:
                        found_index = i

                if found_index:
                    self.connected_devices[serial_port][found_index] = con
                else:
                    self.connected_devices[serial_port].append(con)

                if 'exception' in con:
                    if con['exception'] != '':
                        dev = RegisteredChip.objects.get(mac=con['mac'])
                        dev.connection_exception = con['exception']
                        dev.save()
                    else:
                        dev = RegisteredChip.objects.get(mac=con['mac'])
                        dev.connection_exception = None
                        dev.save()

        print("connected %s" % self.connected_devices)

    # BluetoothServer
    def send_commands_for_devices(self, devices_commands, scheduled=False):

        cmd_dic = {}
        u_id_dic = {}
        for port in self.connected_devices.keys():
            conn_devices = self.connected_devices[port]
            conn_devices_macs = []

            for dev in conn_devices:
                conn_devices_macs.append(dev['mac'])

            print("Conn_devices %s" % conn_devices)
            port_commands = []
            for dc in devices_commands:
                mac = dc['mac']
                if mac in conn_devices_macs:
                    port_commands.append(dc)

            if len(port_commands) > 0:
                cmd_dic[port] = port_commands
                u_id = str(uuid4())
                u_id_dic[port] = u_id
                self.sent_commands_u_ids.append(u_id)

        send_dic = {'action': 'send', 'commands': cmd_dic, 'scheduled': scheduled, 'u_ids': u_id_dic}
        self.communicator.publish_main(send_dic)

        i = 0
        while len(self.sent_commands_u_ids) > 0:
            self.communicator.poll()
            i += 1
            if i > 300:
                break

        self.sent_commands_u_ids = []

        logs = [x for x in self.sent_commands_returned_logs]
        self.sent_commands_returned_logs = []

        return logs

    def received_sent_response(self, log_pks, u_id):
        self.sent_commands_u_ids.remove(u_id)

        for key in log_pks.keys():

            for log in CommandLog.objects.filter(id__in=log_pks[key]):
                self.sent_commands_returned_logs.append(log)
        print('print updated sent_commands %s' % self.sent_commands_returned_logs)

    def process_zmq_message_from_child_any(self, msg):
        if msg.startswith('from_chld'):
            msg = msg.replace('from_chld ||', '')
            dic = json.loads(msg)
            if 'serial_port' in dic:
                print("serial port responded")
                self.serial_port_responded(dic['serial_port'])

            if 'action' in dic:
                action = dic['action']

                if action == 'connected':
                    connect_dic = dic['to_devices']
                    self.client_did_connect_to_devices(connect_dic)

                if action == 'did_send':
                    self.received_sent_response(dic['sent_command_pks'], dic['u_id'])

    def process_zmq_message_from_child(self, msgx):
        print("from_chld %s" % msgx)

        for msg in msgx:
            msg = str(msg, 'utf-8')
            self.process_zmq_message_from_child_any(msg)

    def request_client_serial_port_responses(self):
        print("request_client_serial_port_responses")
        self.communicator.publish_main({'action': 'respond_with_serial_port'})

    def request_client_to_register_to_new_device(self, mac):
        for serial_port in self.connected_devices.keys():
            if mac in self.connected_devices[serial_port]:
                return

        key_list = list(self.connected_devices.keys())
        count = len(key_list)
        searching = False
        times = 0
        serial_port = None

        if count == 0:  # no connect
            if len(self.responded_client_serial_ports) > 0:
                serial_port = self.responded_client_serial_ports[0]
        else:
            while searching is False or times < 1000:
                x = random.randint(0, count - 1)
                times += 1
                sp = key_list[x]

                if len(self.connected_devices[sp]) <= self.max_devices_per_dongle:
                    serial_port = sp
                    searching = True
                    break

                if times >= 1000:
                    return

        if serial_port:
            self.communicator.publish_main({'action': 'connect', 'to_devices': {serial_port: [mac]}})

    def request_clients_to_connect_to_existing_devices(self):
        chips = RegisteredChip.objects.filter()

        if len(chips) == 0 or len(self.responded_client_serial_ports) == 0:
            print("NO CHIPS or SERIAL PORTS %s" % self.responded_client_serial_ports)
            return

        macs = []

        for chip in chips:
            macs.append(chip.mac)

        to_remove = []

        for mac in macs:
            for serial_port in self.connected_devices.keys():
                if mac in self.connected_devices[serial_port]:
                    to_remove.append(mac)
                    break

        for mac in to_remove:
            macs.remove(mac)

        print("len macs %s" % len(macs))
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
        print("len responded_client_serial_ports %s" % len(self.responded_client_serial_ports))
        print("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")

        step = int(math.ceil(len(macs) / len(self.responded_client_serial_ports)))
        print("STEP %s" % step)
        if step > self.max_devices_per_dongle:
            step = self.max_devices_per_dongle
        print("STEP 2 %s" % step)

        seperated_list = [macs[x:x + step] for x in range(0, len(macs), step)]

        print("seperated list %s" % seperated_list)

        connect_dic = {}
        for i, serial_port in enumerate(self.responded_client_serial_ports):
            if i < len(seperated_list):
                connect_dic[serial_port] = seperated_list[i]

        print("CONNECT:::::::::")
        self.communicator.publish_main({'action': 'connect', 'to_devices': connect_dic})


class BluetoothClient(object):

    def __init__(self, serial_port, communicator):
        self.serial_port = serial_port
        self.communicator = communicator
        self.connected_devices = {}
        this = self
        self.bluetooth_handler = BluetoothHandler(this)
        self.bluetooth_handler.start_ble_backend(self.serial_port)
        self.devices_to_retry = set()
        super(BluetoothClient, self).__init__()
        self.is_sending = False

    def check_for_device_availability(self):
        if self.is_sending is True:
            return

        questionable_devices = []
        print("-------------------------------------------------------------------------->")
        print("-------------------------------------------------------------------------->")
        print("-------------------------------------------------------------------------->")
        print("-------------------------------------------------------------------------->")
        print("Check for availability........")

        for c_dev in self.connected_devices.values():
            if c_dev['exception'] is None or c_dev['exception'] == '':
                try:
                    rssi = c_dev['device'].get_rssi()
                    self.bluetooth_handler.check_success(c_dev['device'])
                    print('RSSI %s' % rssi)
                    if rssi == 0:
                        raise Exception("device not found")

                except Exception as e:
                    print("Exception checking device %s    %s" % (c_dev['mac'], e))
                    questionable_devices.append(c_dev['mac'])
            else:

                questionable_devices.append(c_dev['mac'])

        print("QUESTIONABLE DEVICES %s" % questionable_devices)

        for mac in questionable_devices:
            err = self.bluetooth_handler.disconnect_from_device(mac)
            if err:
                print("Error disconnecting from %s %s" % (mac, err))

        print("devices_to_retry %s" % self.devices_to_retry)
        if len(self.devices_to_retry) > 0:
            self.received_connection_request({self.serial_port: self.devices_to_retry})

        if len(questionable_devices) > 0:
            self.received_connection_request({self.serial_port: questionable_devices})
        print("-------------------------------------------------------------------------->")
        print("\n\n")

    def received_connection_request(self, connect_dic):
        print(":::::::::::::::::: Connect dic %s" % connect_dic)
        if self.serial_port in connect_dic:
            conns, exception = self.bluetooth_handler.connect_to_devices_if_necessary(connect_dic[self.serial_port])
            print("CONNNS::::::::::::::::::::::::::::::::::::: %s" % conns)
            print("Exception %s" % exception)

            devices_to_send = {}
            for con_key in conns.keys():
                con = conns[con_key]
                devices_to_send[con_key] = {'mac': con['mac'], 'exception': con['exception']}
                if con['exception'] is not None or con['exception'] != '':
                    self.devices_to_retry.add(con['mac'])
                elif con['mac'] in self.devices_to_retry:
                    self.devices_to_retry.remove(con['mac'])

                self.connected_devices[con_key] = con

            self.communicator.publish_child({'action': 'connected', 'to_devices': {self.serial_port: devices_to_send}})

    # BluetoothClient
    def send_commands(self, commands_by_device, u_id, scheduled):
        self.is_sending = True
        log_pks = self.bluetooth_handler.send_commands(commands_by_device, scheduled)

        send_dic = {'action': 'did_send', 'sent_command_pks': log_pks, 'u_id': u_id}
        self.is_sending = False
        self.communicator.publish_child_push(send_dic)

    def process_zmq_message_from_srv(self, msgx):
        print("from_srv %s" % msgx)
        for msg in msgx:
            msg = str(msg, 'utf-8')
            if msg.startswith('from_srv'):
                msg = msg.replace('from_srv ||', '')
                dic = json.loads(msg)
                if 'action' in dic:
                    action = dic['action']

                    if action == 'respond_with_serial_port':
                        self.communicator.publish_child({'serial_port': self.serial_port})
                    elif action == 'connect':
                        connect_dic = dic['to_devices']
                        self.received_connection_request(connect_dic)

                    elif action == 'send':
                        commands = dic['commands']
                        u_ids = dic['u_ids']
                        scheduled = dic['scheduled']
                        if self.serial_port in commands and self.serial_port in u_ids:
                            self.send_commands(commands[self.serial_port], u_ids[self.serial_port], self)
