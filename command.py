#!/usr/bin/env python
from __future__ import print_function
import os, sys, time
import binascii
import pygatt

import serial


# The BGAPI backend will attemt to auto-discover the serial device name of the
# attached BGAPI-compatible USB adapter.

YOUR_DEVICE_ADDRESS = "FA:86:95:89:1B:05"
ADDRESS_TYPE = pygatt.BLEAddressType.random
YOUR_UUID = '2d30c083-f39f-4ce6-923f-3484ea480596'
YOUR_UUID_read = '2d30c082-f39f-4ce6-923f-3484ea480596'

sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

adapter = pygatt.BGAPIBackend()

if __name__ == '__main__':

    print("===== Start to connect with chip ===== ")

    adapter.start()

    #print("===== adapter.scan() =====")
    #devices = adapter.scan(timeout=5)

#    for dev in devices:
#        # print dev
#        print("address: %s, name: %s " % (dev['address'], dev['name']))

    print("===== adapter.connect() =====")
    connected_device = adapter.connect(YOUR_DEVICE_ADDRESS, address_type=ADDRESS_TYPE)
    print("connection succeed")

    #values = []

    #for character in command_string:
     #   values.append(ord(character))

    #values.append(0)
    values = [116, 0]

    #print("===== send command will be : %s %s ===== " % (command_string, values))

    handle = connected_device.get_handle(YOUR_UUID)
    print("===== Characteristics handle : %s, handle type: %s ===== " % (handle, type(handle)))
    connected_device.char_write_handle(char_handle=handle, value=[116, 0], wait_for_response=True)

    handle_read = connected_device.get_handle(YOUR_UUID_read)
    print("===== Now, reading command (read) =====")

    read_data_from_chip = []
    for i in range(100):
            ret = connected_device.char_read_handle(handle=handle_read, timeout=None)
            ret = ret.decode()
            read_data_from_chip.append(ret)

    print(read_data_from_chip)

    connected_device.char_read(YOUR_UUID_read)

    print("very good!")

    connected_device.disconnect()


