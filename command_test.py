import os, time, sys
import csv
from optparse import OptionParser
import pygatt
from pygatt.backends import BGAPIBackend

# The BGAPI backend will attemt to auto-discover the serial device name of the
# attached BGAPI-compatible USB adapter.

sys.path.append(os.path.abspath(os.path.dirname(os.path.dirname(__file__))))

adapter = pygatt.BGAPIBackend()

if __name__ == '__main__':

    command_string = 't'

    values = []

    for character in command_string:
        values.append(ord(character))

    values.append(0)

    adapter.start()
    device = adapter.connect('D7:86:60:C5:15:2F')
    handle = connected_device.get_handle('2d30c083-f39f-4ce6-923f-3484ea480596')

    connected_device.char_write_handle(char_handle=handle, value=values, wait_for_response=True)

    if self.check_success(connected_device) == False:
        raise Exception("Failed to send command! %s" % command_string)

    print("command string %s" % command_string)

    connected_device.char_write_handle(char_handle=handle, value=values, wait_for_response=True)
    connected_device.disconnect()

